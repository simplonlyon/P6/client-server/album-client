import { ajax } from "jquery";


export class LoginService {
    private apiUrl = 'http://localhost:8080/api/login_check';
    /**
     * Récupère le token dans le sessionStorage
     */
    getToken():string {
        return sessionStorage.getItem('token');
    }

    login(username:string,password:string) {
        return ajax(this.apiUrl, {
            contentType: 'application/json',
            data: JSON.stringify({username:username, password:password}),
            method:'POST'
        })
        .then(resp => sessionStorage.setItem('token', resp.token));
    }

    logout() {
        sessionStorage.removeItem('token');
    }

}